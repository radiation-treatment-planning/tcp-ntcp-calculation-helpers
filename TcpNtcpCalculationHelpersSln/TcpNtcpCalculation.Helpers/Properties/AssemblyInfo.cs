﻿using System.Reflection;
using System.Runtime.InteropServices;

// Allgemeine Informationen über eine Assembly werden über die folgenden
// Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
// die einer Assembly zugeordnet sind.
[assembly: AssemblyTitle("TcpNtcpCalculation.Helpers")]
[assembly: AssemblyDescription("Helpers that extend the functionalities of the Tcp Ntcp calculation project https://gitlab.com/radiation-treatment-planning/tcp-ntcp-calculation.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Medical Center University of Freiburg")]
[assembly: AssemblyProduct("TcpNtcpCalculation.Helpers")]
[assembly: AssemblyCopyright("Copyright © Dejan Kostyszyn 2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Durch Festlegen von ComVisible auf FALSE werden die Typen in dieser Assembly
// für COM-Komponenten unsichtbar.  Wenn Sie auf einen Typ in dieser Assembly von
// COM aus zugreifen müssen, sollten Sie das ComVisible-Attribut für diesen Typ auf "True" festlegen.
[assembly: ComVisible(false)]

// Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
[assembly: Guid("3980dabe-17ff-4818-843b-30c3aa6b3bf9")]

// Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
//
//      Hauptversion
//      Nebenversion
//      Buildnummer
//      Revision
//
// Sie können alle Werte angeben oder Standardwerte für die Build- und Revisionsnummern verwenden,
// indem Sie "*" wie unten gezeigt eingeben:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.1.1")]
[assembly: AssemblyFileVersion("4.1.1")]
