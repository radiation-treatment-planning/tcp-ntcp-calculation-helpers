﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Media.Media3D;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.DVH;
using RadiationTreatmentPlanner.Utils.Patient;

namespace TcpNtcpCalculation.Helpers
{
    public class UPatientsFromFilesCreator
    {
        public IEnumerable<UPatient> CreateFromCumulativeDvhFilesInFolderAndSubfolders(DirectoryInfo rootDirectory)
        {
            var allFiles = FileHelper.GetAllFilesInFolderAndSubfolders(rootDirectory);
            return CreatePatientsFromFiles(allFiles);
        }

        public IEnumerable<UPatient> CreatePatientsFromFiles(IEnumerable<FileInfo> allFiles)
        {
            var patients = new List<UPatient>();
            var defaultStructureSetId = "N/A";
            var defaultCourseId = "N/A";
            var defaultPatientName = "N/A";
            foreach (var fileInfo in allFiles)
            {
                var dvhFileParser = DVHFileParser.CreateFromFile(fileInfo.FullName);
                UPatient patient;
                if (patients.All(x => x.Id != dvhFileParser.GetPiz()))
                {
                    patient = new UPatient(dvhFileParser.GetPiz(), defaultPatientName);
                    patients.Add(patient);
                }
                else
                    patient = patients.First(x => x.Id == dvhFileParser.GetPiz());

                AddAllInformationToPatient(patient, dvhFileParser, defaultStructureSetId, defaultCourseId);
            }
            return patients;

        }

        private void AddAllInformationToPatient(UPatient patient, DVHFileParser dvhFileParser, string defaultStructureSetId, string defaultCourseId)
        {
            var structureSet = AddStructureSetIfNotExists(patient, dvhFileParser, defaultStructureSetId);
            var structure = AddStructureIfNotExists(structureSet, dvhFileParser);
            var course = AddCourseIfNotExists(patient, defaultCourseId);
            var planSetup = AddPlanSetupIfNotExists(course, dvhFileParser, structureSet);
            AddDvhEstimateIfNotExists(planSetup, dvhFileParser, structure);
        }

        private void AddDvhEstimateIfNotExists(UPlanSetup planSetup, DVHFileParser dvhFileParser, UStructure structure)
        {
            planSetup.AddDvhEstimate(structure.Id, dvhFileParser.GetCoverage(), dvhFileParser.GetMinDose(),
                new UDose(0, UDose.UDoseUnit.Unknown), dvhFileParser.GetMedianDose(), dvhFileParser.GetMaxDose(),
                dvhFileParser.GetSamplingCoverage(), dvhFileParser.GetStandardDeviationDose(),
                dvhFileParser.GetTotalVolume(), dvhFileParser.GetDVHCurve());
        }

        private UPlanSetup AddPlanSetupIfNotExists(UCourse course, DVHFileParser dvhFileParser, UStructureSet structureSet)
        {
            var planId = dvhFileParser.GetPlan();
            return course.PlanSetups.All(x => x.Id != planId)
                ? course.AddPlanSetupWithId(planId, Option.Some<uint>(dvhFileParser.GetNumberOfFractions()),
                    dvhFileParser.GetPrescriptionDose(), structureSet)
                : course.PlanSetups.First(x => x.Id == planId);
        }
        private UCourse AddCourseIfNotExists(UPatient patient, string defaultCourseId)
        {
            return patient.Courses.All(x => x.Id != defaultCourseId)
                ? patient.AddCourseWithId(defaultCourseId)
                : patient.Courses.First(x => x.Id == defaultCourseId);
        }

        private UStructure AddStructureIfNotExists(UStructureSet structureSet, DVHFileParser dvhFileParser)
        {
            var structureId = dvhFileParser.GetStructure();
            return structureSet.Structures.All(x => x.Id != structureId)
                ? structureSet.AddStructureWithId(structureId, Option.None<(double, double, double)>(),
                    Option.None<string>(), Option.None<double>(), Option.None<bool>(),
                    Option.None<MeshGeometry3D>())
                : structureSet.Structures.First(x => x.Id == structureId);
        }

        private UStructureSet AddStructureSetIfNotExists(UPatient patient, DVHFileParser dvhFileParser, string defaultStructureSetId)
        {
            return !patient.StructureSets.Any()
                ? patient.AddStructureSetWithId(defaultStructureSetId)
                : patient.StructureSets.First();
        }
    }
}
