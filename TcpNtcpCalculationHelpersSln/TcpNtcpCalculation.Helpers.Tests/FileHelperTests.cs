﻿using System.IO;
using System.Linq;
using NUnit.Framework;

namespace TcpNtcpCalculation.Helpers.Tests
{
    [TestFixture]
    public class FileHelperTests
    {
        [Test]
        public void GetAllFilesInFolder_Test()
        {
            var result = FileHelper.GetAllFilesInFolderAndSubfolders(new DirectoryInfo("TestFolder"));
            var expectedResult = new FileInfo[]
            {
                new FileInfo("CumulativeDvh_patient1_plan1_OAR.txt"),
                new FileInfo("CumulativeDvh_patient1_plan1_GTV.txt"),
                new FileInfo("CumulativeDvh_patient1_plan2_OAR.txt"),
                new FileInfo("CumulativeDvh_patient1_plan2_GTV.txt"),
                new FileInfo("CumulativeDvh_patient2_plan1_OAR.txt"),
                new FileInfo("CumulativeDvh_patient2_plan1_GTV.txt"),
            };

            var expectedStringResult = expectedResult.Select(x => x.Name).ToArray();

            Assert.AreEqual(6, result.Count());
            foreach (var fileInfo in result) Assert.Contains(fileInfo.Name, expectedStringResult);
        }
    }
}
