﻿using TcpNtcpCalculation.Helpers.Immutables;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Helpers
{
    public class TcpNtcpCftcPoint
    {
        public TumorControlProbability TumorControlProbability { get; }
        public NormalTissueComplicationProbability NormalTissueComplicationProbability { get; }
        public ComplicationFreeTumorControl ComplicationFreeTumorControl { get; }
        public UTag Tag { get; }

        public TcpNtcpCftcPoint(TumorControlProbability tumorControlProbability,
            NormalTissueComplicationProbability normalTissueComplicationProbability,
            ComplicationFreeTumorControl complicationFreeTumorControl,
            UTag tag)
        {
            TumorControlProbability = tumorControlProbability;
            NormalTissueComplicationProbability = normalTissueComplicationProbability;
            ComplicationFreeTumorControl = complicationFreeTumorControl;
            Tag = tag;
        }
    }
}
