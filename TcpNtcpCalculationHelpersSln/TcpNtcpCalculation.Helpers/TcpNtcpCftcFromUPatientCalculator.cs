﻿using System;
using System.Collections.Generic;
using System.Linq;
using RadiationTreatmentPlanner.Utils.Patient;
using TcpNtcpCalculation.Helpers.Immutables;
using TcpNtcpCalculation.Immutables;

namespace TcpNtcpCalculation.Helpers
{
    public class TcpNtcpCftcFromUPatientCalculator
    {
        private readonly TcpCalculator _tcpCalculator;
        private readonly NtcpCalculator _ntcpCalculator;
        private readonly ComplicationFreeTumorControlCalculator _complicationFreeTumorControlCalculator;

        public TcpNtcpCftcFromUPatientCalculator(TcpCalculator tcpCalculator,
            NtcpCalculator ntcpCalculator,
            ComplicationFreeTumorControlCalculator complicationFreeTumorControlCalculator)
        {
            _tcpCalculator = tcpCalculator;
            _ntcpCalculator = ntcpCalculator;
            _complicationFreeTumorControlCalculator = complicationFreeTumorControlCalculator;
        }

        public IEnumerable<TcpNtcpCftcPoint> Calculate(UPatient patient, string targetStructureId, string organAtRiskStructureId)
        {
            var tcpNtcpCftcPoints = new List<TcpNtcpCftcPoint>();
            foreach (var course in patient.Courses)
            {
                foreach (var planSetup in course.PlanSetups)
                {
                    var numberOfFractions = planSetup.NumberOfFractions;
                    if (!numberOfFractions.HasValue)
                        throw new ArgumentException(
                            $"PlanSetup with id {planSetup.Id} does not have number of fractions defined. Hence, TCP and NTCP cannot be calculated.");
                    var dvhEstimates = planSetup.DvhEstimates;
                    var dvhOfTargetStructure = dvhEstimates.FirstOrDefault(x => x.Structure.Id == targetStructureId);
                    var dvhOfOrganAtRiskStructure =
                        dvhEstimates.FirstOrDefault(x => x.Structure.Id == organAtRiskStructureId);
                    if (dvhOfTargetStructure is null || dvhOfOrganAtRiskStructure is null) continue;
                    var tcp = _tcpCalculator.Calculate(dvhOfTargetStructure.DvhCurve, numberOfFractions.ValueOr(0));
                    var ntcp = _ntcpCalculator.Calculate(dvhOfOrganAtRiskStructure.DvhCurve, numberOfFractions.ValueOr(0));
                    var tcpNtcpResult = new TcpNtcpResult(tcp, ntcp);
                    var cftc = _complicationFreeTumorControlCalculator.Calculate(tcpNtcpResult);
                    var tag = new UTag(patient.Id, course.Id, planSetup.Id);
                    tcpNtcpCftcPoints.Add(new TcpNtcpCftcPoint(tcp, ntcp,
                        cftc, tag));
                }
            }
            return tcpNtcpCftcPoints;
        }
    }
}
