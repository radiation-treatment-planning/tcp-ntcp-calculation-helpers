﻿using System.IO;

namespace TcpNtcpCalculation.Helpers
{
    public static class FileHelper
    {
        public static FileInfo[] GetAllFilesInFolderAndSubfolders(DirectoryInfo rootDirectory)
        {
            return rootDirectory.GetFiles("*", SearchOption.AllDirectories);
        }
    }
}
