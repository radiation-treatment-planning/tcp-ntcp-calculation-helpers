Helpers that extend the functionalities of the Tcp Ntcp calculation project https://gitlab.com/radiation-treatment-planning/tcp-ntcp-calculation

For examples on how to use the methods see the Unit test project.
