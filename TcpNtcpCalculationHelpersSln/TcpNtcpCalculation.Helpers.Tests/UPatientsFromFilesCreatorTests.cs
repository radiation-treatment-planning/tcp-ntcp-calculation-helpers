﻿using System.IO;
using System.Linq;
using NUnit.Framework;

namespace TcpNtcpCalculation.Helpers.Tests
{
    [TestFixture]
    public class UPatientsFromFilesCreatorTests
    {
        [Test]
        public void CreateFromCumulativeDvhFilesInFolderAndSubfolders()
        {
            var uPatientsFromFilesCreator = new UPatientsFromFilesCreator();
            var patients =
                uPatientsFromFilesCreator.CreateFromCumulativeDvhFilesInFolderAndSubfolders(
                    new DirectoryInfo("TestFolder"));

            Assert.AreEqual(2, patients.Count());
            Assert.AreEqual("patient1", patients.First().Id);
            Assert.AreEqual("patient2", patients.Last().Id);
            Assert.AreEqual(1, patients.First().Courses.Count());
            Assert.AreEqual(1, patients.Last().Courses.Count());

            var structureSetOfPatient1 = patients.First().StructureSets.First();
            var structureSetOfPatient2 = patients.Last().StructureSets.First();
            Assert.AreEqual(2, structureSetOfPatient1.Structures.Count());
            Assert.AreEqual(2, structureSetOfPatient2.Structures.Count());
            Assert.NotNull(structureSetOfPatient1.Structures.FirstOrDefault(x => x.Id == "GTV"));
            Assert.NotNull(structureSetOfPatient1.Structures.FirstOrDefault(x => x.Id == "OAR"));
            Assert.NotNull(structureSetOfPatient2.Structures.FirstOrDefault(x => x.Id == "GTV"));
            Assert.NotNull(structureSetOfPatient2.Structures.FirstOrDefault(x => x.Id == "OAR"));

            Assert.AreEqual(1, patients.First().StructureSets.Count());
            Assert.AreEqual(1, patients.Last().StructureSets.Count());
            Assert.AreEqual("N/A", patients.First().Courses.First().Id);
            Assert.AreEqual("N/A", patients.Last().Courses.First().Id);
            Assert.AreEqual("N/A", patients.First().StructureSets.First().Id);
            Assert.AreEqual("N/A", patients.Last().StructureSets.First().Id);

            var courseOfPatient1 = patients.First().Courses.First();
            var courseOfPatient2 = patients.Last().Courses.First();
            Assert.AreEqual(2, courseOfPatient1.PlanSetups.Count());
            Assert.AreEqual(1, courseOfPatient2.PlanSetups.Count());

            var plan1OfPatient1 = courseOfPatient1.PlanSetups.First();
            var plan2OfPatient1 = courseOfPatient1.PlanSetups.Last();
            var planOfPatient2 = courseOfPatient2.PlanSetups.First();

            Assert.AreEqual("plan1", plan1OfPatient1.Id);
            Assert.AreEqual("plan2", plan2OfPatient1.Id);
            Assert.AreEqual("plan1", planOfPatient2.Id);

            Assert.AreEqual(2, plan1OfPatient1.DvhEstimates.Count());
            Assert.AreEqual(2, plan2OfPatient1.DvhEstimates.Count());
            Assert.AreEqual(2, planOfPatient2.DvhEstimates.Count());
            Assert.NotNull(plan1OfPatient1.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "GTV"));
            Assert.NotNull(plan1OfPatient1.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "OAR"));
            Assert.NotNull(plan2OfPatient1.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "GTV"));
            Assert.NotNull(plan2OfPatient1.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "OAR"));
            Assert.NotNull(planOfPatient2.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "GTV"));
            Assert.NotNull(planOfPatient2.DvhEstimates.FirstOrDefault(x => x.Structure.Id == "OAR"));

        }
    }
}
