﻿using System;

namespace TcpNtcpCalculation.Helpers.Immutables
{
    public class UTag
    {
        public string PatientPiz { get; }
        public string CourseId { get; }
        public string PlanId { get; }
        public string Description { get; private set; }

        public UTag(string patientPiz, string courseId, string planId)
        {
            PatientPiz = patientPiz ?? throw new ArgumentNullException(nameof(patientPiz));
            CourseId = courseId ?? throw new ArgumentNullException(nameof(courseId));
            PlanId = planId ?? throw new ArgumentNullException(nameof(planId));
            Description = $"Patient PIZ\t: {PatientPiz}\n" +
                          $"Course Id\t: {CourseId}\n" +
                          $"Plan Id\t: {PlanId}";
        }

        public void AddKeyValuePair(string key, string value)
        {
            Description += $"\n{key}\t: {value}";
        }
    }
}
