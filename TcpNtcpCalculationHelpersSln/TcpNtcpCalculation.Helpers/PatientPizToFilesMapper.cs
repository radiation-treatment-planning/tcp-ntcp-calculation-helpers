﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TcpNtcpCalculation.Helpers
{
    public class PatientPizToFilesMapper
    {
        private readonly Dictionary<string, List<FileInfo>> _mapper;

        public PatientPizToFilesMapper()
        {
            _mapper = new Dictionary<string, List<FileInfo>>();
        }

        public void Map(string patientPiz, FileInfo filePath)
        {
            if (!_mapper.ContainsKey(patientPiz))
                _mapper.Add(patientPiz, new List<FileInfo>());

            _mapper[patientPiz].Add(filePath);
        }

        public List<FileInfo> GetListOfFilesForPatientWithPiz(string patientPiz)
        {
            return !_mapper.ContainsKey(patientPiz) ? new List<FileInfo>() : _mapper[patientPiz].ToList();
        }

        public IEnumerable<string> GetAvailablePatientPizs()
        {
            return _mapper.Keys.AsEnumerable();
        }
    }
}
