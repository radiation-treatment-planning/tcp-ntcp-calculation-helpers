﻿using System.IO;
using System.Linq;
using NUnit.Framework;

namespace TcpNtcpCalculation.Helpers.Tests
{
    [TestFixture]
    public class PatientPizFromDvhFilesExtractorTests
    {
        [Test]
        public void Extract_Test()
        {
            var patientPizFromDvhFilesExtractor = new PatientPizFromDvhFilesExtractor();
            var allFilePaths = FileHelper.GetAllFilesInFolderAndSubfolders(new DirectoryInfo("TestFolder"));
            var result = patientPizFromDvhFilesExtractor.Extract(allFilePaths);

            var mappedPatients = result.GetAvailablePatientPizs().ToList();
            Assert.AreEqual(2, mappedPatients.Count());
            Assert.Contains("patient1", mappedPatients);
            Assert.Contains("patient2", mappedPatients);
        }
    }
}
