﻿using System.Collections.Generic;
using System.IO;
using RadiationTreatmentPlanner.Utils.DVH;

namespace TcpNtcpCalculation.Helpers
{
    public class PatientPizFromDvhFilesExtractor
    {
        public PatientPizToFilesMapper Extract(IEnumerable<FileInfo> filePaths)
        {
            var patientPizToFilesMapper = new PatientPizToFilesMapper();
            foreach (var path in filePaths)
            {
                var piz = DVHFileParser.ReadOnlyPiz(path.FullName);
                patientPizToFilesMapper.Map(piz, path);
            }

            return patientPizToFilesMapper;
        }
    }
}
