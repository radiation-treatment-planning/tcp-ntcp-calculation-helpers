﻿using System.IO;
using System.Linq;
using NUnit.Framework;

namespace TcpNtcpCalculation.Helpers.Tests
{

    [TestFixture]
    public class PatientPizToFilesMapperTests
    {
        [Test]
        public void GetListOfFilesForPatientWithPizIfAvailable_Test()
        {
            var file1 = new FileInfo("file1");
            var file2 = new FileInfo("file2");
            var file3 = new FileInfo("file3");
            var file4 = new FileInfo("file4");

            var patientPizToFilesMapper = new PatientPizToFilesMapper();
            patientPizToFilesMapper.Map("patient1", file1);
            patientPizToFilesMapper.Map("patient2", file3);
            patientPizToFilesMapper.Map("patient2", file4);
            patientPizToFilesMapper.Map("patient1", file2);

            var filesOfPatient1 = patientPizToFilesMapper.GetListOfFilesForPatientWithPiz("patient1");
            var filesOfPatient2 = patientPizToFilesMapper.GetListOfFilesForPatientWithPiz("patient2");

            Assert.Contains(file1, filesOfPatient1);
            Assert.Contains(file2, filesOfPatient1);
            Assert.Contains(file3, filesOfPatient2);
            Assert.Contains(file4, filesOfPatient2);
        }

        [Test]
        public void GetAvailablePatientPizs_Test()
        {
            var patientPizToFilesMapper = new PatientPizToFilesMapper();
            patientPizToFilesMapper.Map("patient1", new FileInfo("file1"));
            patientPizToFilesMapper.Map("patient2", new FileInfo("file3"));
            patientPizToFilesMapper.Map("patient2", new FileInfo("file4"));
            patientPizToFilesMapper.Map("patient1", new FileInfo("file2"));

            var result = patientPizToFilesMapper.GetAvailablePatientPizs().ToList();
            Assert.AreEqual(2, result.Count);
            Assert.Contains("patient1", result);
            Assert.Contains("patient2", result);
        }

        [Test]
        public void GetAvailablePatientPizs_NoMappings_Test()
        {
            var patientPizToFilesMapper = new PatientPizToFilesMapper();
            Assert.IsEmpty(patientPizToFilesMapper.GetAvailablePatientPizs());
        }
    }
}
