﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Optional;
using RadiationTreatmentPlanner.Utils.Dose;
using RadiationTreatmentPlanner.Utils.Patient;
using TcpNtcpCalculation.Parameters;

namespace TcpNtcpCalculation.Helpers.Tests
{
    [TestFixture]
    public class TcpNtcpCftcFromUPatientCalculatorTests
    {
        private TcpNtcpCftcFromUPatientCalculator _tcpNtcpCftcFromUPatientCalculator;

        [SetUp]
        public void SetUp()
        {
            var tcpNtcpParameterConfig = TcpNtcpParameterConfig.CreateDefaultConfigForPoissonModel();
            var voxelResponseCalculator = new PoissonVoxelResponseCalculator(tcpNtcpParameterConfig.TargetVolume.D50,
                tcpNtcpParameterConfig.TargetVolume.Gamma);
            var tcpCalculator = new TcpCalculator(
                new IsoeffectiveDoseIn2GyConverter(tcpNtcpParameterConfig.TargetVolume.AlphaOverBeta),
                voxelResponseCalculator);
            var ntcpCalculator = new NtcpCalculator(
                new IsoeffectiveDoseIn2GyConverter(tcpNtcpParameterConfig.OrganAtRisk.AlphaOverBeta),
                voxelResponseCalculator, tcpNtcpParameterConfig.OrganAtRisk.DegreeOfSeriality);
            var complicationFreeTumorControlCalculator = new ComplicationFreeTumorControlCalculator();
            _tcpNtcpCftcFromUPatientCalculator = new TcpNtcpCftcFromUPatientCalculator(tcpCalculator, ntcpCalculator,
                complicationFreeTumorControlCalculator);
        }

        [Test]
        public void Calculate_PatientWithNoCourses_Test()
        {
            var patient = new UPatient("123456", "Max Musterfrau");
            var result = _tcpNtcpCftcFromUPatientCalculator.Calculate(patient, "GTV", "OAR");
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void Calculate_PatientWithNoPlanSetups_Test()
        {
            var patient = new UPatient("123456", "Max Musterfrau");
            patient.AddCourseWithId("C1");
            var result = _tcpNtcpCftcFromUPatientCalculator.Calculate(patient, "GTV", "OAR");
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void Calculate_PatientWithNoDvhEstimates_Test()
        {
            var patient = new UPatient("123456", "Max Musterfrau");
            var structreSet = patient.AddStructureSetWithId("ss");
            var course = patient.AddCourseWithId("C1");
            course.AddPlanSetupWithId("plan1", Option.Some<uint>(12), new UDose(62, UDose.UDoseUnit.Gy), structreSet);
            course.AddPlanSetupWithId("plan2", Option.Some<uint>(10), new UDose(64, UDose.UDoseUnit.Gy), structreSet);
            var result = _tcpNtcpCftcFromUPatientCalculator.Calculate(patient, "GTV", "OAR");
            Assert.AreEqual(0, result.Count());
        }

        [Test]
        public void Calculate_Test()
        {
            var uPatientsFromFilesCreator = new UPatientsFromFilesCreator();
            var patients =
                uPatientsFromFilesCreator.CreateFromCumulativeDvhFilesInFolderAndSubfolders(
                    new DirectoryInfo("TestFolder"));
            var patient1 = patients.FirstOrDefault();
            var patient2 = patients.LastOrDefault();

            var result1 = _tcpNtcpCftcFromUPatientCalculator.Calculate(patient1, "GTV", "OAR");
            var result2 = _tcpNtcpCftcFromUPatientCalculator.Calculate(patient2, "GTV", "OAR");

            Assert.AreEqual(2, result1.Count());
            Assert.AreEqual(1, result2.Count());

            var firstTcpNtcpCftcPoint = result1.First();
            var secondTcpNtcpCftcPoint = result1.Last();
            var thirdTcpNtcpCftcPoint = result2.First();
            var tolerance = 0.01;

            Assert.AreEqual(0.955, firstTcpNtcpCftcPoint.TumorControlProbability.Value, tolerance);
            Assert.AreEqual(0.43, firstTcpNtcpCftcPoint.NormalTissueComplicationProbability.Value, tolerance);
            Assert.AreEqual(0.543, firstTcpNtcpCftcPoint.ComplicationFreeTumorControl.Value, tolerance);

            Assert.AreEqual(0.923, secondTcpNtcpCftcPoint.TumorControlProbability.Value, tolerance);
            Assert.AreEqual(0.265, secondTcpNtcpCftcPoint.NormalTissueComplicationProbability.Value, tolerance);
            Assert.AreEqual(0.679, secondTcpNtcpCftcPoint.ComplicationFreeTumorControl.Value, tolerance);

            Assert.AreEqual(0.899, thirdTcpNtcpCftcPoint.TumorControlProbability.Value, tolerance);
            Assert.AreEqual(0.3, thirdTcpNtcpCftcPoint.NormalTissueComplicationProbability.Value, tolerance);
            Assert.AreEqual(0.629, thirdTcpNtcpCftcPoint.ComplicationFreeTumorControl.Value, tolerance);
        }

        [Test]
        public void Calculate_ThrowArgumentException_IfPlanSetupHasNoNumberOfFractionsDefined_Test()
        {
            var patient = new UPatient("123456", "Max Musterfrau");
            var structreSet = patient.AddStructureSetWithId("ss");
            var course = patient.AddCourseWithId("C1");
            course.AddPlanSetupWithId("plan1", Option.None<uint>(), new UDose(62, UDose.UDoseUnit.Gy), structreSet);
            course.AddPlanSetupWithId("plan2", Option.None<uint>(), new UDose(64, UDose.UDoseUnit.Gy), structreSet);
            Assert.Throws<ArgumentException>(() => _tcpNtcpCftcFromUPatientCalculator.Calculate(patient, "GTV", "OAR"));
        }
    }
}
